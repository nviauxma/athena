#include "DerivationFrameworkMuons/dimuonTaggingTool.h"
#include "DerivationFrameworkMuons/isolationDecorator.h"
#include "DerivationFrameworkMuons/mcpDecorator.h"
#include "DerivationFrameworkMuons/MuonTPExtrapolationTool.h"
#include "DerivationFrameworkMuons/IDTrackCaloDepositsDecoratorTool.h"
#include "DerivationFrameworkMuons/MuonIDCovMatrixDecorator.h"
#include "DerivationFrameworkMuons/MuonJetDrTool.h"

DECLARE_COMPONENT( DerivationFramework::dimuonTaggingTool )
DECLARE_COMPONENT( DerivationFramework::isolationDecorator )
DECLARE_COMPONENT( DerivationFramework::mcpDecorator )
DECLARE_COMPONENT( MuonTPExtrapolationTool )
DECLARE_COMPONENT( IDTrackCaloDepositsDecoratorTool )
DECLARE_COMPONENT( DerivationFramework::MuonIDCovMatrixDecorator )
DECLARE_COMPONENT( DerivationFramework::MuonJetDrTool )
